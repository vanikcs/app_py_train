FROM python:3.10
COPY . .
WORKDIR .
CMD /bin/bash -c "make venv && make migrate && make run"
